﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace LASXMLCopier.Model
{
    //[XmlRoot("LAPPL")]
    public class LetterReader
    {
        public string LETTERCTL;
        public string DOCIDNUM;
        public string CLNTNUM;
        public string ContrNo;

       
        public LetterReader(string filename)
        {           
            if (!System.IO.File.Exists(filename))
                return;

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(filename);
            XmlElement root = xmlDoc.DocumentElement;
            XmlNodeList nodeList = root.ChildNodes;

            foreach (XmlNode node in nodeList)
            {
                if (node.Name == "LETTERCTL") this.LETTERCTL = node.InnerText;
                if (node.Name == "DOCIDNUM") this.DOCIDNUM = node.InnerText;
                if (node.Name == "CLNTNUM") this.CLNTNUM = node.InnerText;
                if (node.Name == "ContrNo") this.ContrNo = node.InnerText;
            }
          
        }
    }
}
